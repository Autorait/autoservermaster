import express from 'express';
import { SERVER_PORT } from '../global/environment';
import http from 'http';
import mongoose from 'mongoose';
import { MongoError } from 'mongodb';
import socketIO from 'socket.io';
import * as socket from '../sockets/socket';
// import { MongoError } from 'mongodb';

export default class Server {
  private static _instance: Server;
  public app: express.Application;
  public port: number;

  private httpServer: http.Server;

  public io: socketIO.Server;

  private constructor() {
    this.app = express();
    this.port = SERVER_PORT;

    this.httpServer = new http.Server(this.app);
    this.io = socketIO(this.httpServer);

    this.escucharSockets();

    this.mongoConnect();
  }

  private escucharSockets() {

    console.log('Escuchando conexiones - sockets');

    this.io.on('connection', cliente => {
      // Conectar cliente
      socket.conectarCliente(cliente, this.io);
      // Configurar usuario
      socket.configurarUsuario(cliente, this.io);
      // Obtener usuarios activos
      socket.obtenerUsuarios(cliente, this.io);
      // Mensajes
      socket.mensaje(cliente, this.io);
      // Desconectar
      socket.desconectar(cliente, this.io);
      // Escuchar Posiciones
      socket.escucharPosiciones(cliente, this.io);
      // Configurar emision
      socket.configurarEmision(cliente, this.io);
      // Configurar emision
      socket.obtenerViajes(cliente, this.io);
      // Crear viaje
      socket.crearViaje(cliente, this.io);
      // Aceptar viaje
      socket.aceptarViaje(cliente, this.io);
      // Borrar viaje
      socket.cancelarViaje(cliente, this.io);
      // Recoji a una persona
      socket.recojiConductor(cliente, this.io);
      // Terminar viaje
      socket.finalizarViaje(cliente, this.io);
    });
  }

  public static get instance() {
    return this._instance || (this._instance = new this());
  }

  private mongoConnect() {
    // 
    mongoose.connect(
      'mongodb://localhost:27017/autorait',
      // 'mongodb+srv://marco_diaz:pataPON3@cluster0-jm5fl.mongodb.net/autorait?retryWrites=true&w=majority',
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err: MongoError) => {
        if (err) throw err;
        //console.log('ATLAS conectado.');
      }
    );
  }

  start(callback: Function) {
    this.httpServer.listen(this.port);
    console.log('Corriendo en el puerto ' + this.port);
  }
}
