import { Viajes } from './viajes';


export class ViajesLista {
    private lista: Viajes[] = [];

    constructor() { }

    // Agregar un usuario
    public agregar(viaje: Viajes) {
        this.lista.push(viaje);
        console.log(this.lista);
        return viaje;
    }

    // Obtener lista de viajes
    public getLista() {
        return this.lista.filter(viaje => viaje.estado !== 'END');
    }

    // Obtener un viaje
    public getViaje(id: string) {
        return this.lista.find(viaje => viaje.id === id);
    }

    public cambiarEstado(id: string, estado: string) {
        for (let viaje of this.lista) {
            if (viaje.id === id) {
                viaje.estado = estado;
                break;
            }
        }
    }

    public agregarConductorViaje(id: string, conductor: string, conductorId: string, coche: object) {
        for (let viaje of this.lista) {
            if (viaje.id === id) {
                viaje.conductor = conductor;
                viaje.conductorId = conductorId;
                viaje.coche = coche;
                break;
            }
        }
    }

    // Borrar Usuario
    public borrarViaje(id: string) {
        const tempUsuario = this.getViaje(id);
        this.lista = this.lista.filter(viaje => viaje.id !== id);
        return tempUsuario;
    }
}