import mongoose, { Schema, Document } from 'mongoose';

export interface IRutas extends Document {
    usuario: string
    fromLat: number
    fromLon: number
    fromEscrito: string
    toLat: number
    toLon: number
    toEscrito: string
    inicio: string
    fin: string
    active: number
}

const ConfigSchema: Schema = new Schema({
    usuario: {
        type: String,
        required: [true, 'El campo "usuario" es obligatorio'],
    },
    fromLat: {
        type: Number,
        required: [true, 'El campo "fromLat" es obligatorio'],
    },
    fromLon: {
        type: Number,
        required: [true, 'El campo "fromLon" es obligatorio'],
    },
    fromEscrito: {
        type: String,
        required: [true, 'El campo "fromEscrito" es obligatorio'],
    },
    toLat: {
        type: Number,
        required: [true, 'El campo "toLat" es obligatorio'],
    },
    toLon: {
        type: Number,
        required: [true, 'El campo "toLon" es obligatorio'],
    },
    toEscrito: {
        type: String,
        required: [true, 'El campo "toEscrito" es obligatorio'],
    },
    inicio: {
        type: String,
        required: [true, 'El campo "inicio" es obligatorio'],
    },
    fin: {
        type: String,
        required: [true, 'El campo "fin" es obligatorio'],
    },
    active: {
        type: Boolean,
        default: true,
    },
});

// Export the model and return your IRutas interface
export default mongoose.model<IRutas>('Config', ConfigSchema);