import mongoose, { Schema, Document, SchemaTypes } from 'mongoose';

export interface IDocumentos extends Document {
    ineD: string,
    ineT: string
    licenciaD: string
    licenciaT: string
    tarjeta: string
    poliza: string
}

const DocumentosSchema: Schema = new Schema({
    ineD: {
        type: String,
        default: null,
    },
    ineT: {
        type: String,
        default: null,
    },
    licenciaD: {
        type: String,
        default: null,
    },
    licenciaT: {
        type: String,
        default: null,
    },
    tarjeta: {
        type: String,
        default: null,
    },
    poliza: {
        type: String,
        default: null,
    },
});

// Export he model and return your IDocumentos interface
export default mongoose.model<IDocumentos>('Documentos', DocumentosSchema);