import mongoose, { Schema, Document } from 'mongoose';
import SchemaTypes from 'mongoose';

export interface IUsuarios extends Document {
    curp: string,
    nombre: string
    apellidos: string
    nacimiento: string
    lugarNacimiento: string
    genero: string
    rfc: string
    email: string
    telefono: string
    foto: string
    cp: string
    type: string
    password: string
    documentos: string
    activado: boolean
}

const UsuariosSchema: Schema = new Schema({
    curp: {
        type: Schema.Types.String,
        required: ['true', 'El campo "curp" es obligatorio'],
        unique: true,
    },
    nombre: {
        type: String,
        required: ['true', 'El campo "nombre" es obligatorio'],
    },
    apellidos: {
        type: String,
        required: ['true', 'El campo "apellidos" es obligatorio'],
    },
    nacimiento: {
        type: String,
        required: ['true', 'El campo "nacimiento" es obligatorio'],
    },
    lugarNacimiento: {
        type: String,
        required: ['true', 'El campo "lugarNacimiento" es obligatorio'],
    },
    genero: {
        type: String, // H = Hombre, M = Mujer
        required: ['true', 'El campo "genero" es obligatorio'],
    },
    rfc: {
        type: String,
        required: ['true', 'El campo "rfc" es obligatorio'],
    },
    email: {
        type: Schema.Types.String,
        unique: true,
        required: ['true', 'El campo "email" es obligatorio'],
    },
    telefono: {
        type: Schema.Types.String,
        required: ['true', 'El campo "telefono" es obligatorio'],
        unique: true,
    },
    foto: {
        type: String,
        default: 'NO-IMAGE'
    },
    cp: {
        type: String,
        default: 'NO-CP'
    },
    password: {
        type: String,
        required: ['true', 'El campo "password" es obligatorio'],
    },
    type: {
        type: String,
        default: 'USER_ROLE',
    },
    documentos: {
        type: Schema.Types.ObjectId,
        ref: 'Documentos',
        default: null,
    },
    operando: {
        type: Boolean,
        default: false,
    },
    activado: {
        type: Boolean,
        default: true,
    },

});

// Export he model and return your IUsuarios interface
export default mongoose.model<IUsuarios>('Usuarios', UsuariosSchema);