import { Router, Request, Response } from 'express';
import Server from '../classes/server';
import { usuariosConectados } from '../sockets/socket';

const app = Router();

export { app };

app.get('/status', (req: Request, res: Response) => {
    res.json({
        ok: true,
        data: 'RUNNING'
    });
});

// Servicio para obtener todos los IDs de los usuarios
app.get('/sockets/usuarios', (req: Request, res: Response) => {

    const server = Server.instance;

    server.io.clients((err: any, clientes: string[]) => {
        if (err) {
            return res.json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            clientes
        });
    });
});

// Obtener usuarios y sus nombres
app.get('/sockets/usuarios/detalle', (req: Request, res: Response) => {
    res.json({
        ok: true,
        clientes: usuariosConectados.getLista()
    });
});

export default app;