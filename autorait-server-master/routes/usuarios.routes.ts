import { Request, Response } from 'express';

import { app } from './router';
import bcrypt from 'bcrypt';
import * as _ from 'underscore';

// import UsuariosSchema from '../models/usuarios.model';
import UsuariosSchema from '../models/usuarios.model';
import { MongoError } from 'mongodb';
import nodemailer from 'nodemailer';
// import {
// 	verificaToken,
// 	verificaAdmin_Role
// } from '../middlewares/authentication';


app.get('/usuarios', /*[verificaToken, verificaAdmin_Role],*/(req: Request, res: Response) => {
    UsuariosSchema.find({ activado: true })
        .populate('documentos')
        .sort({ nombre: 1 })
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                data
            });
        });
});

app.get('/usuarios/:id', /*[verificaToken],*/(req: Request, res: Response) => {
    const id = req.params.id;

    UsuariosSchema.findById(id).sort({
        nombre: 1
    })
        .populate('documentos')
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                data
            });
        });
});

app.post('/usuarios', (req: Request, res: Response) => {
    let body = req.body;

    const firstPass = body.password;

    body.email = body.email.trim();
    body.password = bcrypt.hashSync(body.password, 10);

    let values = new UsuariosSchema({
        curp: body.curp,
        nombre: body.nombre,
        apellidos: body.apellidos,
        nacimiento: body.nacimiento,
        lugarNacimiento: body.lugarNacimiento,
        genero: body.genero,
        rfc: body.rfc,
        email: body.email,
        telefono: body.telefono,
        foto: body.foto,
        type: body.type,
        password: body.password,
        documentos: body.documentos,
    });

    UsuariosSchema.create(values, async (err: MongoError, data: any) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        // const transporter = nodemailer.createTransport({
        //     service: 'gmail',
        //     auth: {
        //         user: 'no-reply@autorait.com',
        //         pass: '3M1l1an0'
        //     },
        // });

        // const contentHTML = `
        // <html>

        // <head>
        //     <meta charset="UTF-8">
        //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
        //     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        // </head>

        // <body style="background-color: rgb(244, 244, 244);">
        //     <div style="width: 100%; height: 150px; background-color: rgb(235, 56, 112); padding-top: 30px;">
        //         <div class="container text-center" style="background-color: white; padding: 50px; ">
        //             <h1>Bienvenido a Auto Rait.</h1>
        //             <img src="https://res.cloudinary.com/dhxoolske/image/upload/v1594135186/logo_d1drj6.png " height="60px " class="mt-4 "><br>
        //             <a class="btn btn-primary btn-lg mt-3 " style="background-color: rgb(235, 56, 112); border: none;" href="https://autorait.herokuapp.com/login">Ingresar al Sistema</a>
        //             <p class="mt-3 " style="font-size: 130%; ">Nos complace en darle la bienvenida a nuestro sistema</p>
        //             <div class="text-left mt-5 ">
        //                 <span>Sus credenciales de registro son:</span><br>
        //                 <span><strong>Email:</strong> ${body.email}</span><br>
        //                 <span><strong>Contraseña:</strong> ${firstPass}</span><br>
        //                 <span>Luego de ingresar usted puede cambiar su contraseña en el apartado de perfil.</span>
        //             </div>
        //         </div>
        //         <div class="container text-center mb-5" style="background-color: rgb(235, 56, 112); padding: 30px; ">
        //             <strong style="font-size: 120%; ">¿Nesesitas ayuda?</strong><br>
        //             <span style="color: white; text-decoration: underline; ">Sigue este enlace y podemos contestar tus preguntas</span>
        //         </div>
        //     </div>
        // </body>

        // </html>
        // `;

        // const info = await transporter.sendMail({
        //     from: "'Administración Auto Rait' <no-reply@autorait.com>",
        //     to: data.email,
        //     subject: 'Bienvenido a Auto Rait',
        //     html: contentHTML,
        // });

        res.json({
            ok: true,
            data,
            // email: info.messageId
        });
    });
});

app.post('/usuarios/reset/sendMail', async (req: Request, res: Response) => {
    let body = req.body;

    let password = await UsuariosSchema.findOne({ activado: true, email: body.email }, 'password').exec();
    let sendKey = password?.password;

    let sendKey1;

    if (sendKey) {
        sendKey1 = encodeURIComponent(sendKey);
    }

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'no-reply@autorait.com',
            pass: '3M1l1an0'
        },
    });

    const contentHTML = `
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    </head>
    
    <body style="background-color: rgb(244, 244, 244);">
        <div style="width: 100%; height: 150px; background-color: rgb(235, 56, 112); padding-top: 30px;">
            <div class="container text-center" style="background-color: white; padding: 50px; ">
                <h1>Administración de cuentas Auto Rait.</h1>
                <img src="https://res.cloudinary.com/dhxoolske/image/upload/v1594135186/logo_d1drj6.png " height="60px " class="mt-4 "><br>
                <!-- <a class="btn btn-primary btn-lg mt-3 " style="background-color: rgb(235, 56, 112); border: none;" href="https://autorait.herokuapp.com/api/usuarios">Cambiar la contraseña</a> -->
                <a class="btn btn-primary btn-lg mt-3 " style="background-color: rgb(235, 56, 112); border: none;" href="https://autorait.herokuapp.com/reset/${sendKey1}">Cambiar la contraseña</a>
                <p class="mt-3 " style="font-size: 130%; ">Oprima este boton para que pueda seleccionar una nueva contraseña para su cuenta</p>
                <div class="text-left mt-5 ">
                    <span>Si usted no reconoce este movimiento, no dude en enviar un mensaje a nuestro equipo de soporte:</span><br>
                    <span><strong>Correo electronico:</strong> ${body.email}</span><br>
                </div>
            </div>
            <div class="container text-center mb-5" style="background-color: rgb(235, 56, 112); padding: 30px; ">
                <strong style="font-size: 120%; ">¿Nesesitas ayuda?</strong><br>
                <span style="color: white; text-decoration: underline; ">Sigue este enlace y podemos contestar tus preguntas</span>
            </div>
        </div>
    </body>
    
    </html>
    `;

    const info = await transporter.sendMail({
        from: "'Administración Auto Rait' <no-reply@autorait.com>",
        to: body.email,
        subject: 'Cambiar contraseña de Auto Rait',
        html: contentHTML,
    });

    res.json({
        ok: true,
        message: 'Email enviado a tu cuenta',
    })
});

app.post('/usuarios/changePassword/', /*[verificaToken],*/async (req: Request, res: Response) => {

    let body = req.body;

    let user = await UsuariosSchema.findOne({ activado: true, password: body.password0 }, '_id').exec();
    let id = user?._id;

    if (id === null) {
        return res.json({
            ok: false,
            err: 'Esta sesión a expirado',
        });
    }

    const bodyN = {
        password: bcrypt.hashSync(body.password1, 10),
    }

    UsuariosSchema.findByIdAndUpdate(id, bodyN, { new: true, runValidators: true }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data: data
        });
    });
});

app.put('/usuarios/:id', /*[verificaToken],*/async (req: Request, res: Response) => {

    const id = req.params.id;

    let body = req.body;

    UsuariosSchema.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data: data
        });
    });
});

app.delete('/usuarios/:id', /*[verificaToken, verificaAdmin_Role],*/(req: Request, res: Response) => {
    let id = req.params.id;

    UsuariosSchema.findByIdAndUpdate(id, { activado: false }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data: data
        });
    });
});

export default app;