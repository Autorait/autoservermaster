import { Request, Response } from 'express';

import { app } from './router';
import * as _ from 'underscore';

// import UsuariosSchema from '../models/usuarios.model';
import DocumentosSchema from '../models/documentos.model';
import { MongoError } from 'mongodb';
import nodemailer from 'nodemailer';
// import {
// 	verificaToken,
// 	verificaAdmin_Role
// } from '../middlewares/authentication';


app.get('/documentos', /*[verificaToken, verificaAdmin_Role],*/(req: Request, res: Response) => {
    DocumentosSchema.find().exec((err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data
        });
    });
});

app.get('/documentos/:id', /*[verificaToken],*/(req: Request, res: Response) => {
    const id = req.params.id;

    DocumentosSchema.findById(id)
        .exec((err, data) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            res.json({
                ok: true,
                data
            });
        });
});

app.post('/documentos', (req: Request, res: Response) => {
    let body = req.body;

    let values = new DocumentosSchema({
        curp: body.curp,
        nombre: body.nombre,
        apellidos: body.apellidos,
        nacimiento: body.nacimiento,
        lugarNacimiento: body.lugarNacimiento,
        genero: body.genero,
        rfc: body.rfc,
        email: body.email,
        telefono: body.telefono,
        foto: body.foto,
        type: body.type,
        password: body.password
    });

    DocumentosSchema.create(values, async (err: MongoError, data: any) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data,
        });
    });
});

app.put('/documentos/:id', /*[verificaToken],*/async (req: Request, res: Response) => {

    const id = req.params.id;

    let body = req.body;

    DocumentosSchema.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data: data
        });
    });
});

app.delete('/usuarios/:id', /*[verificaToken, verificaAdmin_Role],*/(req: Request, res: Response) => {
    let id = req.params.id;

    DocumentosSchema.findByIdAndUpdate(id, { activado: false }, (err, data) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            data: data
        });
    });
});

export default app;