import { Router, Request, Response } from 'express';
import fileUpload, { UploadedFile } from 'express-fileupload';
import fs from 'fs';

import { app } from './router';
import path from 'path';
import vision from '@google-cloud/vision';
import Shell = require('shelljs');
import cloudinary = require('cloudinary');


app.use(fileUpload());

const client = new vision.ImageAnnotatorClient({
    keyFilename: 'vision-api.json'
});

app.put('/upload/:type', (req: Request, res: Response) => {
    const type = req.params.type;

    const pathImg = path.resolve(__dirname, `../uploads`);
    if (!fs.existsSync(pathImg)) {
        Shell.mkdir('-p', pathImg);
    }

    if (!req.files) {
        return res.status(400).json({
            ok: 'false',
            err: 'No se a seleccionado ningun archivo'
        });
    }

    let types = ['user_profile'];

    if (types.indexOf(type) < 0) {
        return res.status(400).json({
            ok: 'false',
            err: 'El archivo no es un tipo valido'
        });
    }

    let file = req.files.file as UploadedFile;
    let nombreCortado = file.name.split('.');
    let ex = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    let extensiones = ['png', 'jpg', 'jpeg'];

    if (extensiones.indexOf(ex) < 0) {
        return res.status(400).json({
            ok: 'false',
            err: 'El archivo tiene una extension no invalida'
        });
    }

    const date = new Date();
    let nombreSubir = `${type}${date.getFullYear()}${date.getMonth()}${date.getDay()}${date.getHours()}${date.getSeconds()}${date.getMilliseconds()}`;


    //file.mv(`dist/uploads/${nombreSubir}.${ex}`, (err) => {
    file.mv(`${path.resolve(__dirname, `../uploads`)}/${nombreSubir}.${ex}`, async (err) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }

        return res.json({
            ok: true,
            message: 'Subido correctamente',
            name: `${nombreSubir}.${ex}`
        });
    });
});

app.put('/upload/document/:id/:noDoc', (req: Request, res: Response) => {

    const pathImg = path.resolve(__dirname, `../uploads`);
    if (!fs.existsSync(pathImg)) {
        Shell.mkdir('-p', pathImg);
    }

    const noDoc = req.params.noDoc;
    const id = req.params.id;

    const nombre = req.body.nombre;

    if (!req.files) {
        return res.status(400).json({
            ok: 'false',
            err: 'No se a seleccionado ningun archivo'
        });
    }

    let file = req.files.file as UploadedFile;
    let nombreCortado = file.name.split('.');
    let ex = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    let extensiones = ['png', 'jpg', 'jpeg', 'pdf'];

    if (extensiones.indexOf(ex) < 0) {
        return res.status(400).json({
            ok: 'false',
            err: 'El archivo tiene una extension no invalida'
        });
    }

    let nombreSubir = `document${id}&${noDoc}`;


    file.mv(`${path.resolve(__dirname, `../uploads`)}/${nombreSubir}.${ex}`, async (err) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }

        if (noDoc === '0' || noDoc === '2') {

            const results1 = await client.textDetection(`${path.resolve(__dirname, `../uploads`)}/${nombreSubir}.${ex}`);
            const textos = results1[0].fullTextAnnotation?.text;

            if (textos) {
                if (!(textos.indexOf(nombre) > 0)) {
                    borrarArchivo(`${nombreSubir}.${ex}`);
                    return res.json({
                        ok: false,
                        message: 'No paso la validación',
                    });
                }
            } else {
                borrarArchivo(`${nombreSubir}.${ex}`);
                return res.json({
                    ok: false,
                    message: 'No paso la validación',
                });
            }

            console.log('YES')

            const results2 = await client.labelDetection(`${path.resolve(__dirname, `../uploads`)}/${nombreSubir}.${ex}`);
            const labels = results2[0].labelAnnotations;

            const validPhoto = ['Identity document', 'Forehead', 'License'];

            if (labels) {
                labels.forEach(async (label) => {
                    if (validPhoto.indexOf((label.description == null) ? '' : label.description) > 0) {
                        return res.json({
                            ok: true,
                            message: 'Subido correctamente',
                            name: `${nombreSubir}.${ex}`
                        });
                    }
                });

                borrarArchivo(`${nombreSubir}.${ex}`);
                return res.json({
                    ok: false,
                    message: 'No paso la validación',
                });
            }
        } else {
            return res.json({
                ok: true,
                message: 'Subido correctamente',
                name: `${nombreSubir}.${ex}`
            });
        }
    });
});

app.get('/uploads/:filename', (req: Request, res: Response) => {
    const filename = req.params.filename;
    let pathImg = path.resolve(__dirname, `../uploads/${filename}`)

    if (fs.existsSync(pathImg)) {
        res.sendFile(pathImg);
    } else {
        res.status(400).json({
            ok: false,
            message: 'Imagen no encontrada',
            pathImg
        });
    }
});

const borrarArchivo = (url: string) => {
    const pathImg = path.resolve(__dirname, `../uploads/${url}`);
    fs.unlinkSync(pathImg);
}

export default app;