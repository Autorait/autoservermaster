import Server from './classes/server';
import bodyParser from 'body-parser';
import cors from 'cors';
import express from 'express';
import path from 'path';

// Rutas
import router from './routes/router';
import usuarios from './routes/usuarios.routes';
import login from './routes/login.routes';
import upload from './routes/uploads.routes';
import documentos from './routes/documentos.routes';
import marcas from './routes/marcas.routes';
import vehiculos from './routes/vehiculos.routes';
import viajes from './routes/viajes.routes';
import config from './routes/config.routes';

const server = Server.instance;

// BodyParser
server.app.use(bodyParser.urlencoded({ extended: true }));
server.app.use(bodyParser.json());

// ANGULAR
// server.app.use(express.static('public'));

// CORS
server.app.use(cors({ origin: true, credentials: true }));

// Rutas de MongoDB
server.app.use('/', (express.static('public', { redirect: false })));
// Rutas
server.app.use('/api', router);
server.app.use('/api', usuarios);
server.app.use('/api', login);
server.app.use('/api', upload);
server.app.use('/api', documentos);
server.app.use('/api', marcas);
server.app.use('/api', vehiculos);
server.app.use('/api', viajes);
server.app.use('/api', config);
// 
server.app.get('*', (req, res, next) => {
  res.sendFile(path.resolve('public/index.html'));
});



server.start(() => {
  console.log(`Servidor corriendo en el puerto ${server.port}`);
});