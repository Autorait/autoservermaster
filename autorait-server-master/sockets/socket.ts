import { Socket } from 'socket.io';
import socketIO from 'socket.io';
import { UsuariosLista } from '../classes/usuarios-lista';
import { Usuario } from '../classes/usuario';
import { ViajesLista } from '../classes/viajes-lista';
import { Viajes } from '../classes/viajes';
import axios from 'axios';

export const usuariosConectados = new UsuariosLista();
export const viajes = new ViajesLista();


export const conectarCliente = (cliente: Socket, io: socketIO.Server) => {

    const usuario = new Usuario(cliente.id);
    usuariosConectados.agregar(usuario);

}


export const desconectar = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('disconnect', () => {
        console.log('Cliente desconectado');

        usuariosConectados.borrarUsuario(cliente.id);

        io.emit('usuarios-activos', usuariosConectados.getLista());

    });
}


// Escuchar mensajes
export const mensaje = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('mensaje', (payload: { de: string, cuerpo: string }) => {
        console.log('Mensaje recibido', payload);
        io.emit('mensaje-nuevo', { ...payload, de: cliente.id });
    });
}

// Configurar usuario
export const configurarUsuario = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('configurar-usuario', (payload: { nombre: string, tipo: string, idDatabase: string, nToken: string }) => {
        usuariosConectados.actualizarNombre(cliente.id, payload.nombre, payload.idDatabase, payload.tipo, payload.nToken);
        io.emit('usuarios-activos', usuariosConectados.getLista());
    });
}

// Configurar usuario
export const configurarEmision = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('configurar-emision', (payload, callback: Function) => {
        usuariosConectados.configurarEmision(cliente.id, payload.emitiendo, payload.tipo);
        io.emit('actualiza-marcador', usuariosConectados.getUsuario(cliente.id));
    });
}


// Obtener Usuarios
export const obtenerUsuarios = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('obtener-usuarios', () => {
        io.to(cliente.id).emit('usuarios-activos', usuariosConectados.getLista());
    });
}

// Actualizar posicion
export const escucharPosiciones = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('emitir-posicion', (data) => {
        usuariosConectados.actualizaPosicion(cliente.id, data.lat, data.lon);
        io.emit('actualiza-marcador', usuariosConectados.getUsuario(cliente.id));
        //console.log(usuariosConectados.getUsuario(cliente.id))
    });
}

export const obtenerViajes = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('obtenerViajes', () => {
        io.to(cliente.id).emit('lista-viajes', viajes.getLista());
    });
}

// Lo emite el pasajero
export const crearViaje = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('crear-viaje', (payload: { id: string, usuario: string, usuarioId: string, usuarioFoto: string, fromLat: number, fromLon: number, toLat: number, toLon: number, fromEscrito: string, toEscrito: string, precio: number, distancia: number, comentarios: string }) => {
        const viaje = new Viajes(payload.id, payload.usuario, payload.usuarioId, payload.usuarioFoto, payload.fromLat, payload.fromLon, payload.toLat, payload.toLon, payload.fromEscrito, payload.toEscrito, payload.precio, payload.distancia, payload.comentarios);
        viajes.agregar(viaje);
        console.log('✅🚖  Viaje agregado');
        const emitirViaje = viajes.getViaje(payload.id);
        io.emit('nuevo-viaje', emitirViaje);
    });
}

// Lo emite el conductor
export const aceptarViaje = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('aceptar-viaje', (payload: { id: string, conductor: string, conductorId: string, coche: object }) => {
        console.log(payload)
        const viaje = viajes.getViaje(payload.id);
        console.log(viaje);
        viajes.cambiarEstado(viaje?.id!, 'ACCEPTED')
        viajes.agregarConductorViaje(viaje?.id!, payload.conductor, payload.conductorId, payload.coche);

        console.log(usuariosConectados.verIdDeSocket(viaje?.usuario!)!);

        io.to(usuariosConectados.verIdDeSocket(viaje?.usuario!)!).emit('viaje-aceptado', viajes.getViaje(payload.id));
    });
}

export const cancelarViaje = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('cancelar-viaje', (payload: { id: string }) => {
        io.emit('viaje-cancelado', viajes.getViaje(payload.id),);
        viajes.borrarViaje(payload.id);
        console.log('❌🚖  Viaje eliminado');
    });
}

export const recojiConductor = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('recoji-conductor', (payload: { id: string }) => {
        const viaje = viajes.getViaje(payload.id);
        viajes.cambiarEstado(viaje?.id!, 'INPROGRESS')

        usuariosConectados.verIdLista(viaje?.usuarioId!).forEach((e) => {
            io.to(e).emit('fuiste-recojido', viajes.getViaje(payload.id));
        });
    });
}

export const finalizarViaje = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('terminar-viaje', (payload: { id: string }) => {
        const viaje = viajes.getViaje(payload.id);
        viajes.cambiarEstado(viaje?.id!, 'END')
        io.to(usuariosConectados.verIdDeSocket(viaje?.usuario!)!).emit('tu-viaje-termino', viajes.getViaje(payload.id));
        io.to(usuariosConectados.verIdDeSocket(viaje?.conductorId!)!).emit('tu-viaje-termino', viajes.getViaje(payload.id));
    });
}

// Incidencia policial
export const incidenciaPolicial = (cliente: Socket, io: socketIO.Server) => {
    cliente.on('ayuda', (payload: { nombre: string, lat: string, lon: string, speed: string }) => {

        const lista = usuariosConectados.getLista();

        lista.forEach((e) => {
            if (e.nombre === 'POLICIA') {
                io.emit('policia', {
                    nombre: payload.nombre,
                    lat: payload.lat,
                    lon: payload.lon,
                    speed: payload.speed,
                });
            }
        });

        // io.to(cliente.id).emit('usuarios-activos', usuariosConectados.getLista());

    });
}